pub mod block_chain {
    use crate::logblock::block::{Block, Superblock, EntryBlockTyp};
    use std::error::Error;
    use std::fmt;
    use std::fmt::Formatter;
    use crate::logblock::block::{Entries, BlockHash};
    use serde::{Deserialize, Serialize};
    use chrono::Utc;

    const MAX_ENTRIES: usize = 5;

    #[derive(Debug, Serialize, Deserialize)]
    pub struct SuperBlockchain {
        chain: Vec<Superblock>,
    }


    impl SuperBlockchain {
        pub fn new() -> Self {
            SuperBlockchain {
                chain: vec![],
            }
        }
        pub fn get_chain(&self) -> &Vec<Superblock> {
            &self.chain
        }
        pub fn get_last_index(&self) -> u32 {
            match self.chain.last() {
                None => { 0 }
                Some(block) => { block.index }
            }
        }
        pub fn get_last_blockhash(&self) -> BlockHash {
            match self.chain.last() {
                None => { vec![0; 64] }
                Some(last_block) => { last_block.hash_block() }
            }
        }
        pub fn add_block(&mut self, terminal_block: Block) -> Result<(), BlockchainError> {
            let timestamp = Utc::now().timestamp();
            let (last_index, last_timestamp) = match self.chain.last().ok_or(BlockchainError::BlockchainEmpty) {
                Ok(block) => { (block.index, block.timestamp) }
                Err(_) => { (u32::MIN, i64::MIN) }
            };
            if last_index > self.get_last_index() {
                return Err(BlockchainError::LowerIndexThanExpected);
            } else if timestamp <= last_timestamp {
                return Err(BlockchainError::TimestampIsOlderThanFromLastBlock);
            }
            let new_block = Superblock::new(last_index + 1, timestamp, self.get_last_blockhash(), terminal_block);
            self.chain.push(new_block);
            Ok(())
        }
        pub fn verify(&self) -> Result<(), BlockchainError> {
            let mut prev_hash: BlockHash = vec![0; 64];
            let mut prev_index: u32 = 0;
            let mut prev_timestamp: i64 = i64::MIN;
            for block in &self.chain {
                if block.prev_hash.ne(&prev_hash) || block.index < prev_index || block.timestamp < prev_timestamp {
                    return Err(BlockchainError::VerifyFailed(block.index));
                } else {
                    prev_hash = block.hash_block();
                    prev_index = block.index;
                    prev_timestamp = block.timestamp;
                }
            }
            Ok(())
        }
    }


    #[derive(Debug, Serialize, Deserialize)]
    pub struct BlockChain {
        chain: Vec<Block>,
    }

    impl BlockChain {
        pub fn new() -> Self {
            BlockChain {
                chain: vec![]
            }
        }
        pub fn is_on_end(&self) -> bool {
            self.chain.len() >= MAX_ENTRIES - 1
        }
        pub fn is_empty(&self) -> bool {
            self.chain.is_empty()
        }
        pub fn get_chain(&self) -> &Vec<Block> {
            &self.chain
        }
        pub fn get_last_block_hash(&self) -> Option<BlockHash> {
            match self.chain.last() {
                Some(block) => Some(block.hash_block()),
                None => None
            }
        }
        pub fn verify(&self) -> Result<(), BlockchainError> {
            let mut prev_hash: BlockHash = vec![0; 64];
            let mut prev_index: u32 = 0;
            let mut prev_timestamp: i64 = i64::MIN;
            for block in &self.chain {
                if block.prev_hash.ne(&prev_hash) || (block.index != 0 && block.index != prev_index + 1) || block.timestamp < prev_timestamp {
                    return Err(BlockchainError::VerifyFailed(block.index));
                } else {
                    prev_hash = block.hash_block();
                    prev_index = block.index;
                    prev_timestamp = block.timestamp;
                }
            }
            Ok(())
        }

        pub fn add_block(&mut self, index: u32, timestamp: i64, entries: Entries) -> Result<CircledBlockchainInfo, BlockchainError> {
            let (last_index, last_timestamp, is_empty) = match self.chain.last().ok_or(BlockchainError::BlockchainEmpty) {
                Ok(block) => { (block.index, block.timestamp, false) }
                Err(_) => { (u32::MIN, i64::MIN, true) }
            };
            if last_index > index {
                return Err(BlockchainError::LowerIndexThanExpected);
            } else if last_index + 1 < index {
                return Err(BlockchainError::HigherIndexThanExpected);
            } else if timestamp <= last_timestamp {
                return Err(BlockchainError::TimestampIsOlderThanFromLastBlock);
            }
            if is_empty {
                let gen_res = self.genesis_block(index, timestamp, entries);
                return match gen_res {
                    Ok(_) => {
                        Ok(CircledBlockchainInfo::GenesisBlockAdded)
                    }
                    Err(err) => { Err(err) }
                };
            } else if self.is_on_end()
            {
                let end_res = self.terminal_block(index, timestamp);
                return match end_res {
                    Ok(_) => { Ok(CircledBlockchainInfo::NeedNewCircledBlockchain(entries)) }
                    Err(err) => { Err(err) }
                };
            } else {
                let new_block = Block::new_normal(index, timestamp, self.chain.last().unwrap().hash_block(), entries);
                self.chain.push(new_block);
            }
            Ok(CircledBlockchainInfo::NothingToDo)
        }
        pub fn genesis_block(&mut self, index: u32, timestamp: i64, entries: Entries) -> Result<(), BlockchainError> {
            if self.is_empty() {
                let new_block = Block::new_genesis(index, timestamp, entries);
                self.chain.push(new_block);
                Ok(())
            } else {
                Err(BlockchainError::CircledBlockchainNotAtStart)
            }
        }
        pub fn terminal_block(&mut self, index: u32, timestamp: i64) -> Result<(), BlockchainError> {
            if self.is_on_end() {
                match self.get_last_block_hash() {
                    None => {
                        // Das sollte nicht passieren
                        Err(BlockchainError::CircledBlockchainStart)
                    }
                    Some(hash) => {
                        let new_block = Block::new_terminal(index, timestamp, hash);
                        self.chain.push(new_block);
                        Ok(())
                    }
                }
            } else {
                Err(BlockchainError::CircledBlockchainNotAtEnd)
            }
        }
        pub fn get_terminal_block(&self) -> Option<Block> {
            let last_block = self.chain.last();
            match last_block {
                None => { None }
                Some(block) => {
                    if EntryBlockTyp::Terminal.eq(&block.typ) {
                        let t_block = Block::new_terminal(block.index, block.timestamp, block.prev_hash.clone());
                        Some(t_block)
                    } else { None }
                }
            }
        }
        //eine Funktion für genisi und terminal blöcke
    }

    pub enum CircledBlockchainInfo {
        NothingToDo,
        NeedNewCircledBlockchain(Entries),
        GenesisBlockAdded,
    }

    #[derive(Debug)] // Allow the use of "{:?}" format specifier
    pub enum BlockchainError {
        HigherIndexThanExpected,
        LowerIndexThanExpected,
        TimestampIsOlderThanFromLastBlock,
        VerifyFailed(u32),
        BlockchainEmpty,
        CircledBlockchainEnd,
        CircledBlockchainStart,
        CircledBlockchainNotAtStart,
        CircledBlockchainNotAtEnd,

    }


    // Allow the use of "{}" format specifier
    impl fmt::Display for BlockchainError {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            match *self {
                BlockchainError::HigherIndexThanExpected => write!(f, "The index of the given block higher than the expected index."),
                BlockchainError::LowerIndexThanExpected => write!(f, "The index of the given block lower than the expected index."),
                BlockchainError::TimestampIsOlderThanFromLastBlock => write!(f, "The given Timestamp is older than from the last block."),
                BlockchainError::VerifyFailed(index) => write!(f, "In the block with the index {} the verify failed.", index),
                BlockchainError::BlockchainEmpty => write!(f, "Blockchain is empty."),
                BlockchainError::CircledBlockchainEnd => write!(f, "CircledBlockchain is at Maximum."),
                BlockchainError::CircledBlockchainStart => write!(f, "CircledBlockchain is at start."),
                BlockchainError::CircledBlockchainNotAtStart => { write!(f, "CircledBlockchain is not at start.") }
                BlockchainError::CircledBlockchainNotAtEnd => { write!(f, "CircledBlockchain is not at Maximum.") }
            }
        }
    }

    impl Error for BlockchainError {}
}

pub mod blockchain_wrapper {
    use crate::logblockchain::block_chain::{BlockChain, BlockchainError, SuperBlockchain, CircledBlockchainInfo};
    use std::fs::{File, OpenOptions};
    use crate::logblock::block::{Block, BlockHash, Entries};
    use std::path::{Path, PathBuf};
    use std::io::{Write, Read};
    use futures::io::Error;

    #[derive(Debug)] // Allow the use of "{:?}" format specifier
    pub enum BlockchainFileError {
        EmptyFile,
        NoFile,
        ErrorReading,
    }

    pub struct BlockchainWrapper {
        blockchain: BlockChain,
        file: File,
    }

    pub struct SuperBlockchainWrapper {
        superblockchain: SuperBlockchain,
        file: File,
        path_of: PathBuf,
        current_subchain: BlockchainWrapper,
    }

    impl SuperBlockchainWrapper {
        pub(crate) fn verify_sub_chain(&self) -> Result<(), BlockchainError> {
            self.current_subchain.verify()
        }
        pub(crate) fn verify_superblockchain(&self) -> Result<(), BlockchainError> {
            self.superblockchain.verify()
        }
        pub fn complete_verify(self) -> Result<(), BlockchainError> {
            let sub_chain_res = self.verify_sub_chain();
            let super_chain_res = self.verify_superblockchain();
            for block in self.superblockchain.get_chain() {
                let mut bl_path = self.path_of.clone();
                bl_path.push(format!("{}.chain", block.index));
                let blw = BlockchainWrapper::new_with_error(bl_path.as_path());
                match blw {
                    Ok(bl) => {
                        match bl.verify() {
                            Ok(_) => {}
                            Err(err) => { return Err(err); }
                        }
                    }

                    Err(err) => {
                        match err {
                            BlockchainFileError::EmptyFile => { println!("File is empty") }
                            BlockchainFileError::NoFile => { println!("File seems deleted") }
                            BlockchainFileError::ErrorReading => { println!("Error reading/parse file") }
                        }
                    }
                }
            }
            Ok(())
        }
        pub fn get_last_index(&self) -> Option<u32> {
            let li = self.superblockchain.get_last_index();
            let li2 = self.current_subchain.get_last_index();
            match li2 {
                None => { None }
                Some(index) => {
                    Some(index.max(li))
                }
            }
        }
        pub fn new(path: &Path) -> SuperBlockchainWrapper {
            let tmp = OpenOptions::new().
                write(true).
                read(true).
                create(true).open(path);
            match tmp {
                Ok(mut file) => {
                    let mut tmp_vec = vec![];
                    let res = file.read_to_end(&mut tmp_vec);
                    let superblockchain = match res {
                        Ok(0) => { SuperBlockchain::new() }
                        Ok(_x) => { serde_cbor::from_slice(tmp_vec.as_slice()).expect("Error in SuperBlockchain") }
                        Err(_) => { panic!("Can't load SuperBlockchain") }
                    };
                    let mut path_to_currentsubchain = path.to_path_buf();
                    let mut path2 = path.to_path_buf();
                    path2.pop();
                    path_to_currentsubchain.pop();
                    path_to_currentsubchain.push(format!("{}.chain", superblockchain.get_last_index()));
                    SuperBlockchainWrapper {
                        superblockchain,
                        file,
                        path_of: path2,
                        current_subchain: BlockchainWrapper::new(path_to_currentsubchain.as_path()),
                    }
                }
                Err(_err) => panic!("Can't open SuperBlockchain")
            }
        }
        pub(crate) fn add_block(&mut self, index: u32, timestamp: i64, entries: Entries) -> Result<(), BlockchainError> {
            let res = self.current_subchain.add_block(index, timestamp, entries);
            let real_res = match res {
                Ok(ok) => {
                    match ok {
                        CircledBlockchainInfo::NothingToDo => { Ok(()) }
                        CircledBlockchainInfo::NeedNewCircledBlockchain(entries) => {
                            let teminal_block_op = self.current_subchain.get_terminal_block();
                            match teminal_block_op {
                                None => { return Err(BlockchainError::CircledBlockchainNotAtEnd); }
                                Some(block) => {
                                    let super_bl_add_res = self.superblockchain.add_block(block);
                                    if let Err(err) = super_bl_add_res {
                                        return Err(err);
                                    }
                                }
                            }
                            let mut path_to_currentsubchain = self.path_of.clone();
                            path_to_currentsubchain.push(format!("{}.chain", self.superblockchain.get_last_index()));
                            self.current_subchain = BlockchainWrapper::new(path_to_currentsubchain.as_path());
                            match self.current_subchain.add_block(index + 1, timestamp, entries) {
                                Ok(ook) => {
                                    match ook {
                                        CircledBlockchainInfo::NothingToDo => { Ok(()) }
                                        CircledBlockchainInfo::NeedNewCircledBlockchain(_) => { Err(BlockchainError::CircledBlockchainEnd)/* Can't be the case */ }
                                        CircledBlockchainInfo::GenesisBlockAdded => { Ok(()) }
                                    }
                                }
                                Err(err2) => { Err(err2) }
                            }//Null oder weiterzähelen
                        }
                        CircledBlockchainInfo::GenesisBlockAdded => { Ok(())/* Everything alright, but index is + 2 */ }
                    }
                }
                Err(err) => { Err(err) }
            };
            let ress = self.file.write_all(serde_cbor::to_vec(&self.superblockchain).unwrap().as_slice());
            match ress {
                Ok(_) => { /*Nothing to do*/ }
                Err(_) => { error!("Error writing Blockchain file"); }
            };
            real_res
        }
        pub fn get_last_block_hash(&self) -> BlockHash {
            self.current_subchain.get_last_block_hash()
        }
    }

    impl BlockchainWrapper {
        pub fn new(path: &Path) -> BlockchainWrapper {
            let tmp = OpenOptions::new().
                write(true).
                read(true).
                create(true).open(path);
            match tmp {
                Ok(mut file) => {
                    let mut tmp_vec = vec![];
                    let res = file.read_to_end(&mut tmp_vec);
                    let bl = match res {
                        Ok(0) => { BlockChain::new() }
                        Ok(_x) => { serde_cbor::from_slice(tmp_vec.as_slice()).expect("Error in Blockchain") }
                        Err(_) => { panic!("Can't load Blockchain") }
                    };
                    BlockchainWrapper {
                        blockchain: bl,
                        file,
                    }
                }
                Err(_err) => panic!("Can't open Blockchain")
            }
        }
        pub fn new_with_error(path: &Path) -> Result<BlockchainWrapper, BlockchainFileError> {
            let tmp = OpenOptions::new().
                write(true).
                read(true).
                create(true).open(path);
            match tmp {
                Ok(mut file) => {
                    let mut tmp_vec = vec![];
                    let res = file.read_to_end(&mut tmp_vec);
                    let bl = match res {
                        Ok(0) => { Err(BlockchainFileError::EmptyFile) }
                        Ok(_x) => {
                            match serde_cbor::from_slice(tmp_vec.as_slice()) {
                                Ok(bloc) => Ok(bloc),
                                Err(_) => Err(BlockchainFileError::ErrorReading)
                            }
                        }
                        Err(_) => { Err(BlockchainFileError::NoFile) }
                    };
                    match bl {
                        Ok(chain) => {
                            Ok(
                                BlockchainWrapper {
                                    blockchain: chain,
                                    file,
                                })
                        }
                        Err(err) => { Err(err) }
                    }
                }
                Err(_err) => Err(BlockchainFileError::NoFile)
            }
        }
        pub fn get_chain(&self) -> &Vec<Block> {
            &self.blockchain.get_chain()
        }
        pub fn get_last_block_hash(&self) -> BlockHash {
            match self.blockchain.get_chain().last() {
                Some(block) => block.hash_block(),
                None => vec![0; 64]
            }
        }
        pub fn get_last_index(&self) -> Option<u32> {
            match self.blockchain.get_chain().last() {
                Some(block) => Some(block.index),
                None => None,
            }
        }

        pub(crate) fn verify(&self) -> Result<(), BlockchainError> {
            self.blockchain.verify()
        }
        pub fn get_terminal_block(&self) -> Option<Block> {
            self.blockchain.get_terminal_block()
        }
        //noch eine test funktion, wleche hash erwaertet wird
        pub(crate) fn add_block(&mut self, index: u32, timestamp: i64, entries: Entries) -> Result<CircledBlockchainInfo, BlockchainError> {
            let res = self.blockchain.add_block(index, timestamp, entries);
            let ress = self.file.write_all(serde_cbor::to_vec(&self.blockchain).unwrap().as_slice());
            match ress {
                Ok(_) => { /*Nothing to do*/ }
                Err(_) => { error!("Error writing Blockchain file"); }
            };
            res
        }
    }
}
pub mod logchain_messages {
    use serde::{Deserialize, Serialize};
    use libp2p::identity::PublicKey;
    use libp2p::identity::Keypair;
    use libp2p::identity::error::SigningError;
    use sha3::{Digest, Sha3_512};
    use crate::logblock::block::{Entry, BlockHash};
    use libp2p::PeerId;
    use std::str::FromStr;
    use chrono::Utc;
    use std::fmt::{Display, Formatter};
    use core::fmt;


    #[derive(Debug, Serialize, Deserialize, Clone)]
    pub struct RequestMessage {
        /// The timestamp of the Block in non-leap-milliseconds since January 1, 1970 UTC.
        pub timestamp: i64,
        pub entries: Vec<Entry>,
        pub id: u32,
    }

    impl RequestMessage {
        pub fn new(entries: Vec<Entry>) -> RequestMessage {
            RequestMessage {
                timestamp: Utc::now().timestamp(),
                entries,
                id: rand::random(),
            }
        }
        pub fn to_outer_message(&self, keypair: &Keypair) -> Result<OuterMessage, SigningError> {
            let mut message = OuterMessage {
                message_typ: MessageTyp::Request,
                message: serde_cbor::to_vec(self).unwrap(),
                message_authentication_code: vec![],
                message_signature: vec![],
            };
            message.set_mac();
            let res = message.sign(keypair);
            match res {
                Ok(_) => { Ok(message) }
                Err(err) => { Err(err) }
            }
        }
        pub fn message_mac(&self) -> Vec<u8> {
            let mut hasher = Sha3_512::new();
            let data = serde_cbor::to_vec(&self);
            hasher.update(data.unwrap());
            hasher.finalize().to_vec()
        }
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct PrePrepareMessage {
        #[serde(with = "serde_bytes")]
        pub(crate) last_block_hash: BlockHash,
        pub(crate) actual_index: u32,
        #[serde(with = "serde_bytes")]
        pub(crate) mac: Vec<u8>,
        pub(crate) message: RequestMessage,

    }

    impl PrePrepareMessage {
        pub fn new(last_block_hash: BlockHash, next_index: u32, message: RequestMessage) -> Self {
            PrePrepareMessage {
                last_block_hash,
                actual_index: next_index,
                mac: message.message_mac(),
                message,
            }
        }
        pub fn to_outer_message(&self, keypair: &Keypair) -> Result<OuterMessage, SigningError> {
            let mut message = OuterMessage {
                message_typ: MessageTyp::PrePrepare,
                message: serde_cbor::to_vec(self).unwrap(),
                message_authentication_code: vec![],
                message_signature: vec![],
            };
            message.set_mac();
            let res = message.sign(keypair);
            match res {
                Ok(_) => { Ok(message) }
                Err(err) => { Err(err) }
            }
        }
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct PrepareMessage {
        #[serde(with = "serde_bytes")]
        pub(crate) last_block_hash: BlockHash,
        pub(crate) actual_index: u32,
        pub(crate) peer: String,
    }

    impl PrepareMessage {
        pub fn new(last_block_hash: BlockHash, actual_index: u32, peer: PeerId) -> Self {
            PrepareMessage {
                last_block_hash,
                actual_index,
                peer: peer.to_base58(),
            }
        }
        pub fn to_outer_message(&self, keypair: &Keypair) -> Result<OuterMessage, SigningError> {
            let mut message = OuterMessage {
                message_typ: MessageTyp::Prepare,
                message: serde_cbor::to_vec(self).unwrap(),
                message_authentication_code: vec![],
                message_signature: vec![],
            };
            message.set_mac();
            let res = message.sign(keypair);
            match res {
                Ok(_) => { Ok(message) }
                Err(err) => { Err(err) }
            }
        }
        pub fn get_peer_id(&self) -> PeerId {
            PeerId::from_str(&self.peer).unwrap()
        }
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct CommitMessage {
        #[serde(with = "serde_bytes")]
        pub(crate) new_block_hash: BlockHash,
        pub(crate) new_index: u32,
    }

    impl CommitMessage {
        pub fn new(new_index: u32, new_block_hash: BlockHash) -> Self {
            CommitMessage {
                new_block_hash,
                new_index,
            }
        }
        pub fn to_outer_message(&self, keypair: &Keypair) -> Result<OuterMessage, SigningError> {
            let mut message = OuterMessage {
                message_typ: MessageTyp::Commit,
                message: serde_cbor::to_vec(self).unwrap(),
                message_authentication_code: vec![],
                message_signature: vec![],
            };
            message.set_mac();
            let res = message.sign(keypair);
            match res {
                Ok(_) => { Ok(message) }
                Err(err) => { Err(err) }
            }
        }
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub enum Replies {
        ErrorOccurred,
        EntryProcessed,
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct ReplyMessage {
        pub id: u32,
        pub reply: Replies,
    }

    impl ReplyMessage {
        pub fn new_okay(id: u32) -> ReplyMessage {
            ReplyMessage {
                id,
                reply: Replies::EntryProcessed,
            }
        }
        pub fn new_err(id: u32) -> ReplyMessage {
            ReplyMessage {
                id,
                reply: Replies::ErrorOccurred,
            }
        }
        pub fn to_outer_message(&self, keypair: &Keypair) -> Result<OuterMessage, SigningError> {
            let mut message = OuterMessage {
                message_typ: MessageTyp::Reply,
                message: serde_cbor::to_vec(self).unwrap(),
                message_authentication_code: vec![],
                message_signature: vec![],
            };
            message.set_mac();
            let res = message.sign(keypair);
            match res {
                Ok(_) => { Ok(message) }
                Err(err) => { Err(err) }
            }
        }
        pub fn message_mac(&self) -> Vec<u8> {
            let mut hasher = Sha3_512::new();
            let data = serde_cbor::to_vec(&self);
            hasher.update(data.unwrap());
            hasher.finalize().to_vec()
        }
    }

    #[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
    pub enum MessageTyp {
        Request,
        PrePrepare,
        Prepare,
        Commit,
        Reply,
    }

    impl Display for MessageTyp {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            match self {
                MessageTyp::Request => { write!(f, "Request") }
                MessageTyp::PrePrepare => { write!(f, "PrePrepare") }
                MessageTyp::Prepare => { write!(f, "Prepare") }
                MessageTyp::Commit => { write!(f, "Commit") }
                MessageTyp::Reply => { write!(f, "Reply") }
            }
        }
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct OuterMessage {
        pub message_typ: MessageTyp,
        #[serde(with = "serde_bytes")]
        pub(crate) message: Vec<u8>,
        #[serde(with = "serde_bytes")]
        pub message_authentication_code: Vec<u8>,
        #[serde(with = "serde_bytes")]
        pub(crate) message_signature: Vec<u8>,
    }

    impl OuterMessage {
        pub fn get_message_typ(&self) -> &MessageTyp {
            &self.message_typ
        }
        /// If the outer message contains PrePrepareMessage it returns it in Some else None
        pub fn to_pre_prepare_message(&self) -> Option<PrePrepareMessage> {
            match self.message_typ {
                MessageTyp::PrePrepare => {
                    let pre_prepare_message: Result<PrePrepareMessage, _> = serde_cbor::from_slice(self.message.as_slice());
                    match pre_prepare_message {
                        Result::Ok(messsage) => Option::Some(messsage),
                        Result::Err(_) => None
                    }
                }
                _ => None,
            }
        }
        /// If the outer message contains PrepareMessage it returns it in Some else None
        pub fn to_prepare_message(&self) -> Option<PrepareMessage> {
            match self.message_typ {
                MessageTyp::Prepare => {
                    let prepare_message: Result<PrepareMessage, _> = serde_cbor::from_slice(self.message.as_slice());
                    match prepare_message {
                        Result::Ok(messsage) => Option::Some(messsage),
                        Result::Err(_) => None
                    }
                }
                _ => None,
            }
        }
        /// If the outer message contains CommitMessage it returns it in Some else None
        pub fn to_commit_message(&self) -> Option<CommitMessage> {
            match self.message_typ {
                MessageTyp::Commit => {
                    let commit_message: Result<CommitMessage, _> = serde_cbor::from_slice(self.message.as_slice());
                    match commit_message {
                        Result::Ok(messsage) => Option::Some(messsage),
                        Result::Err(_) => None
                    }
                }
                _ => None,
            }
        }
        /// If the outer message contains RequestMessage it returns it in Some else None
        pub fn to_request_message(&self) -> Option<RequestMessage> {
            match self.message_typ {
                MessageTyp::Request => {
                    let request_message: Result<RequestMessage, _> = serde_cbor::from_slice(self.message.as_slice());
                    match request_message {
                        Result::Ok(messsage) => Option::Some(messsage),
                        Result::Err(_) => None
                    }
                }
                _ => None,
            }
        }
        /// If the outer message contains ReplyMessage it returns it in Some else None
        pub fn to_reply_message(&self) -> Option<ReplyMessage> {
            match self.message_typ {
                MessageTyp::Reply => {
                    let reply_message: Result<ReplyMessage, _> = serde_cbor::from_slice(self.message.as_slice());
                    match reply_message {
                        Result::Ok(messsage) => Option::Some(messsage),
                        Result::Err(_) => None
                    }
                }
                _ => None,
            }
        }
        pub fn verify(&self, public_key: &PublicKey) -> bool {
            public_key.verify(self.message.as_slice(), self.message_signature.as_slice())
        }
        pub fn sign(&mut self, keypair: &Keypair) -> Result<(), SigningError> {
            let signature = keypair.sign(self.message.as_slice());
            match signature {
                Ok(sig) => {
                    self.message_signature = sig;
                    Ok(())
                }
                Err(sig_err) => Err(sig_err)
            }
        }
        pub fn set_mac(&mut self) {
            let mut hasher = Sha3_512::new();
            hasher.update(self.message.as_slice());
            self.message_authentication_code = hasher.finalize().to_vec();
        }
        pub fn calculate_mac(&self) -> Vec<u8> {
            let mut hasher = Sha3_512::new();
            hasher.update(self.message.as_slice());
            hasher.finalize().to_vec()
        }
    }
}

pub mod logchain_context {
    use super::logchain_messages::{RequestMessage, MessageTyp};
    use crate::logchainnetwork::logchain_messages::{OuterMessage, PrePrepareMessage, PrepareMessage, CommitMessage, ReplyMessage};
    use std::collections::HashMap;
    use libp2p::identity::{PublicKey, Keypair};
    use libp2p::PeerId;
    use libp2p::floodsub::{Topic, Floodsub};
    use crate::logblockchain::blockchain_wrapper::{BlockchainWrapper, SuperBlockchainWrapper};
    use std::fs::File;
    use std::fs;
    use std::path::Path;
    use std::io::Read;
    use libp2p::identity::ed25519::{PublicKey as EDPublicKey};
    use serde::{Deserialize, Serialize};
    use mio::Token;
    use std::sync::{Mutex, Arc};
    use std::fmt::{Display, Formatter};
    use libp2p::core::upgrade::MapInboundUpgrade;
    use crate::logblock::block::printHex_Blockhash;


    #[derive(PartialEq, Eq)]
    pub enum ContextState {
        Start,
        Prepare,
        Commit,
        Reply,
    }

    impl Display for ContextState {
        fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
            match self {
                ContextState::Start => { write!(f, "Start") }
                ContextState::Prepare => { write!(f, "Prepare") }
                ContextState::Commit => { write!(f, "Commit") }
                ContextState::Reply => { write!(f, "Reply") }
            }
        }
    }

    pub enum ContextError {
        NotExpectedMessage,
        MessageAuthenticationCodeCheckFailed,
        VerifyFailed,
        ProbablyMaliciousMessage,
        SigningFailed,
    }

    pub enum ContextReturn {
        StateUp,
        ProbablyOldMessage,
    }

    pub struct LogChainContext {
        chain: SuperBlockchainWrapper,
        pub state: ContextState,
        own_key_pair: Keypair,
        public_keys: Vec<PublicKey>,
        received_prepare_messages: Vec<PrepareMessage>,
        received_commit_messages: Vec<CommitMessage>,
        request_message: Option<RequestMessage>,
        pre_prepare_message: Option<PrePrepareMessage>,
        prepare_message: Option<PrepareMessage>,
        commit_message: Option<CommitMessage>,
        topic: Topic,
        topic_client: Topic,
        possible_faulty_nodes: u8,
        response_list: Arc<Mutex<Vec<Token>>>,
        split_message_map: HashMap<(PeerId, u32), (Vec<Vec<u8>>, u8)>,
        primary_node: PeerId,
        node_priority: Vec<(PeerId, bool)>,
        pub mutex: Arc<Mutex<u8>>,
        message_id: u32,
    }

    //Because Floodsub donsen't support messages > ~1400 Byte
    #[derive(Debug, Serialize, Deserialize)]
    pub struct SplitMessage {
        pub id: u32,
        pub index: u8,
        pub end: u8,
        #[serde(with = "serde_bytes")]
        pub data: Vec<u8>,
    }

    pub const MAX_BYTES: usize = 1200;

    pub fn my_floodsub_publish(topic: &Topic, floodsub: &mut Floodsub, data: Vec<u8>, id: u32) {
        let mut i = 0;
        let data_to_send = data.chunks(MAX_BYTES);
        let end = data_to_send.len() as u8;
        for chunk in data_to_send {
            let split_message = SplitMessage {
                id,
                index: i,
                end,
                data: Vec::from(chunk),
            };
            i = i + 1;
            let bytes_to_send = serde_cbor::to_vec(&split_message);
            match bytes_to_send {
                Ok(bytes) => {
                    floodsub.publish(topic.clone(), bytes);
                }
                Err(_) => { println!("Parsing error") }
            }
        }
    }

    impl LogChainContext {
        pub fn new(chain: SuperBlockchainWrapper, own_key_pair: Keypair, topic: Topic, topic_client: Topic, pub_key_path: &Path, response_list: Arc<Mutex<Vec<Token>>>, primary: String, node_pri: Vec<String>) -> LogChainContext {
            let mut public_keys = vec![];
            let pub_dir = fs::read_dir(pub_key_path);
            for file in pub_dir.unwrap() {
                let new_pub_key_file = File::open(file.unwrap().path());
                match new_pub_key_file {
                    Ok(mut pub_file) => {
                        let mut pub_key_as_bytes: Vec<u8> = vec![];
                        pub_file.read_to_end(&mut pub_key_as_bytes).unwrap_or(0);
                        let pk = EDPublicKey::decode(pub_key_as_bytes.as_slice());
                        match pk {
                            Ok(ppk) => {
                                public_keys.push(PublicKey::Ed25519(ppk));
                            }
                            Err(_) => println!("Error loading file as pub key")
                        }
                    }
                    Err(_) => println!("Error loading file as pub key")
                }
            }
            let mut node_priority = vec![];
            let mut primary_node = PeerId::random();
            for base_str in &node_pri {
                for pubkey in &public_keys {
                    let peerid_base = pubkey.clone().into_peer_id().to_base58();
                    if peerid_base.eq(base_str) {
                        node_priority.push((pubkey.clone().into_peer_id(), true));
                    }
                    if peerid_base.eq(&primary) {
                        primary_node = pubkey.clone().into_peer_id();
                    }
                }
            }
            println!("Primary node_new Context: {}", primary_node.to_base58());
            if node_priority.len() != node_pri.len() {
                panic!("Public keys don't match priority order.")
            }

            LogChainContext {
                chain,
                response_list,
                state: ContextState::Start,
                own_key_pair: own_key_pair.clone(),
                public_keys,
                received_prepare_messages: vec![],
                received_commit_messages: vec![],
                request_message: None,
                pre_prepare_message: None,
                prepare_message: None,
                commit_message: None,
                topic: topic.clone(),
                topic_client: topic_client.clone(),
                possible_faulty_nodes: 1,
                split_message_map: HashMap::default(),
                primary_node,
                node_priority,
                mutex: Arc::new(Mutex::new(0)),
                message_id: 0,
            }
        }

        pub fn process_message(&mut self, message: &OuterMessage, floodsub: &mut Floodsub) -> Result<ContextReturn, ContextError> {
            println!("Bin {}/{}", self.state, message.message_typ);
            if message.calculate_mac().ne(&message.message_authentication_code) {
                return Err(ContextError::MessageAuthenticationCodeCheckFailed);
            } else if !message.message_typ.eq(&MessageTyp::Request) && self.public_keys.iter().map(|x| message.verify(x)).filter(|x| *x).collect::<Vec<bool>>().len() != 1 {
                return Err(ContextError::VerifyFailed);
            }
            match self.state {
                ContextState::Start => {
                    match message.get_message_typ() {
                        MessageTyp::Request => {
                            let request_message = message.to_request_message();
                            let res = self.process_request_message(request_message.unwrap(), floodsub);
                            match res {
                                Ok(ok) => { Ok(ok) }
                                Err(err) => { Err(err) }
                            }
                        }
                        MessageTyp::PrePrepare => {
                            let pre_prepare_message = message.to_pre_prepare_message();
                            self.process_pre_prepare_message(pre_prepare_message.unwrap(), floodsub)
                        }
                        MessageTyp::Commit => {
                            if message.to_commit_message().unwrap().new_index < self.chain.get_last_index().unwrap_or(0) {
                                Ok(ContextReturn::ProbablyOldMessage)
                            } else {
                                self.reset_state();
                                Err(ContextError::ProbablyMaliciousMessage)
                            }
                        }
                        MessageTyp::Prepare => {
                            if message.to_prepare_message().unwrap().actual_index <= self.chain.get_last_index().unwrap_or(0) {
                                Ok(ContextReturn::ProbablyOldMessage)
                            } else {
                                self.reset_state();//vielleicht nicht die beste idee
                                Err(ContextError::ProbablyMaliciousMessage)
                            }
                        }
                        _ => Err(ContextError::NotExpectedMessage)
                    }
                }
                ContextState::Prepare => {
                    match message.get_message_typ() {
                        MessageTyp::Prepare => {
                            let prepare_message = message.to_prepare_message();
                            let res = self.process_prepare_message(prepare_message.unwrap(), floodsub);
                            match res {
                                Ok(ok) => { Ok(ok) }
                                Err(err) => { Err(err) }
                            }
                        }
                        _ => Err(ContextError::NotExpectedMessage)
                    }
                }
                ContextState::Commit => {
                    match message.get_message_typ() {
                        MessageTyp::Commit => {
                            let commit_message = message.to_commit_message();
                            self.process_commit_message(commit_message.unwrap(), floodsub);
                            Ok(ContextReturn::StateUp)
                        }
                        _ => Err(ContextError::NotExpectedMessage)
                    }
                }
                ContextState::Reply => {
                    match message.get_message_typ() {
                        MessageTyp::Reply =>
                            Ok(ContextReturn::StateUp),
                        _ => Err(ContextError::NotExpectedMessage)
                    }
                }
            }
        }
        pub fn is_in_start(&self) -> bool {
            self.state.eq(&ContextState::Start)
        }

        pub fn append_split_message(&mut self, mut split_message: SplitMessage, peer: PeerId) -> Option<OuterMessage> {
            let is_data = self.split_message_map.get_mut(&(peer, split_message.id));
            match is_data {
                None => {
                    let mut tmp_vec = vec![vec![]; split_message.end as usize];
                    if let Some(elem) = tmp_vec.get_mut(split_message.index as usize) {
                        *elem = split_message.data;
                    }
                    self.split_message_map.insert((peer,split_message.id), (tmp_vec, 1));
                }
                Some(data) => {
                    if let Some(elem) = data.0.get_mut(split_message.index as usize) {
                        *elem = split_message.data;
                        data.1 = data.1 + 1;
                    }
                }
            }
            if self.split_message_map.get(&(peer, split_message.id)).unwrap_or(&(vec![vec![0 as u8]], 0 as u8)).1 == (split_message.end) {
                let outer_message_data_opt = self.split_message_map.get_mut(&(peer, split_message.id));
                match outer_message_data_opt {
                    None => { println!("No Data") }
                    Some(mut data) => {
                        let mut datas = vec![];
                        data.0.iter_mut().for_each(|ele| datas.append(ele));
                        let outer_mess = serde_cbor::from_slice::<OuterMessage>(datas.as_slice());
                        match outer_mess {
                            Ok(message) => {
                                self.split_message_map.remove(&(peer, split_message.id));
                                return Some(message);
                            }
                            Err(_) => println!("Error parsing")
                        }
                    }
                }
            }
            None
        }
        pub fn get_topic_clone(&self) -> Topic {
            self.topic.clone()
        }
        pub fn get_topic_client_clone(&self) -> Topic {
            self.topic_client.clone()
        }
        pub fn set_peer_offline(&mut self, peer: PeerId) {
            for mut peer_and_status in &mut self.node_priority {
                if peer_and_status.0.eq(&peer) {
                    peer_and_status.1 = false;
                }
            }
            if self.primary_node.eq(&peer) {
                self.primary_node = self.node_priority.iter().filter(|x| x.1).next().unwrap().0.clone();
            }
        }
        pub fn am_i_primary(&self) -> bool {
            self.own_key_pair.public().into_peer_id().eq(&self.primary_node)
        }
        pub fn increment_message_id(&mut self) -> u32 {
            let re = self.message_id;
            self.message_id = re + 1;
            re
        }
        pub fn set_peer_online(&mut self, peer: PeerId) {
            for mut peer_and_status in &mut self.node_priority {
                if peer_and_status.0.eq(&peer) {
                    peer_and_status.1 = true;
                }
            }
        }
        pub fn process_request_message(&mut self, message: RequestMessage, floodsub: &mut Floodsub) -> Result<ContextReturn, ContextError> {
            let pre_prepare_message = PrePrepareMessage::new(self.chain.get_last_block_hash(), self.chain.get_last_index().unwrap_or(0), message.clone());
            self.request_message = Some(message);
            let outer_message_res = pre_prepare_message.to_outer_message(&self.own_key_pair);
            match outer_message_res {
                Ok(outer_message) => {
                    self.pre_prepare_message = Some(pre_prepare_message);
                    self.prepare_message = Some(PrepareMessage::new(self.chain.get_last_block_hash(),
                                                                    self.chain.get_last_index().unwrap_or(0),
                                                                    self.own_key_pair.public().into_peer_id()));
                    let message_id = self.increment_message_id();
                    my_floodsub_publish(&self.topic, floodsub, serde_cbor::to_vec(&outer_message).unwrap(), message_id);
                    self.state = ContextState::Prepare;
                    Ok(ContextReturn::StateUp)
                }
                Err(err) => {
                    Err(ContextError::SigningFailed)
                }
            }
        }
        fn process_pre_prepare_message(&mut self, message: PrePrepareMessage, floodsub: &mut Floodsub) -> Result<ContextReturn, ContextError> {
            if self.request_message.is_none() {
                self.request_message = Some(message.message.clone())
            }
            let prepare_message = PrepareMessage::new(self.chain.get_last_block_hash(),
                                                      self.chain.get_last_index().unwrap_or(0),
                                                      self.own_key_pair.public().into_peer_id());
            let result_outer_message = prepare_message.to_outer_message(&self.own_key_pair);
            match result_outer_message {
                Ok(outer_message) => {
                    self.prepare_message = Some(prepare_message);
                    self.pre_prepare_message = Some(message);
                    let message_id = self.increment_message_id();
                    my_floodsub_publish(&self.topic, floodsub, serde_cbor::to_vec(&outer_message).unwrap(), message_id);
                    self.state = ContextState::Prepare;
                    Ok(ContextReturn::StateUp)
                }
                Err(err) => {
                    Err(ContextError::SigningFailed)
                }
            }
        }
        fn process_prepare_message(&mut self, message: PrepareMessage, floodsub: &mut Floodsub) -> Result<ContextReturn, ContextError> {
            match self.prepare_message.as_ref() {
                None => {
                    return Err(ContextError::NotExpectedMessage);
                }
                Some(own_prepare_message) => {
                    if own_prepare_message.last_block_hash.eq(&message.last_block_hash) && own_prepare_message.actual_index == message.actual_index
                    {
                        self.received_prepare_messages.push(message);
                    }
                    if self.received_prepare_messages.len() >= (2 * self.possible_faulty_nodes) as usize {
                        match self.pre_prepare_message.as_ref() {
                            None => { return Err(ContextError::NotExpectedMessage); }
                            Some(own_pre_prepare_message) => {
                                let new_index = own_pre_prepare_message.actual_index + 1;
                                let check = self.chain.add_block(new_index, own_pre_prepare_message.message.timestamp, own_pre_prepare_message.message.entries.clone());
                                print!("Block addded new index {}, Hash: ", new_index);
                                printHex_Blockhash(self.chain.get_last_block_hash());
                                println!();
                                let commit_message = CommitMessage::new(new_index, self.chain.get_last_block_hash());
                                let message_to_send = commit_message.to_outer_message(&self.own_key_pair);
                                if let Err(err) = message_to_send {
                                    return Err(ContextError::SigningFailed);
                                } else {
                                    let bytes_to_send = serde_cbor::to_vec(&message_to_send.unwrap()).unwrap();
                                    let message_id = self.increment_message_id();
                                    my_floodsub_publish(&self.topic, floodsub, bytes_to_send, message_id);
                                    self.commit_message = Some(commit_message);
                                    self.state = ContextState::Commit;
                                }
                            }
                        }
                    }
                }
            }
            Ok(ContextReturn::StateUp)
        }


        fn process_commit_message(&mut self, message: CommitMessage, floodsub: &mut Floodsub) -> Result<ContextReturn, ContextError> {
            let tmp_commit_message = self.commit_message.as_ref().unwrap();
            if tmp_commit_message.new_index == message.new_index && tmp_commit_message.new_block_hash.eq(&message.new_block_hash) {
                self.received_commit_messages.push(message);
            }
            if self.received_commit_messages.len() >= (2 * self.possible_faulty_nodes) as usize {
                let req_id = match &self.request_message {
                    None => { 0 }
                    Some(req_message) => { req_message.id }
                };
                let message_to_send = ReplyMessage::new_okay(req_id).to_outer_message(&self.own_key_pair);
                if let Err(err) = message_to_send {
                    return Err(ContextError::SigningFailed);
                } else {
                    let bytes_to_send = serde_cbor::to_vec(&message_to_send.unwrap()).unwrap();
                    let message_id = self.increment_message_id();
                    my_floodsub_publish(&self.topic_client, floodsub, bytes_to_send, message_id);
                    self.reset_state();
                }
            }
            Ok(ContextReturn::StateUp)
        }
        pub fn reset_state(&mut self) -> () {
            self.pre_prepare_message = None;
            self.prepare_message = None;
            self.received_commit_messages.clear();
            self.received_prepare_messages.clear();
            self.commit_message = None;
            self.request_message = None;
            self.state = ContextState::Start;
            println!("State reset")
        }
        fn print_state_message(&self) -> () {
            match self.state {
                ContextState::Start => { println!("Bin in Start") }
                ContextState::Prepare => { println!("Bin in Prepare") }
                ContextState::Commit => { println!("Bin in Commit") }
                ContextState::Reply => { println!("Bin in Reply") }
            }
            println!("Habe {} Prepare Messages und {} CommitMessages empfangen.", self.received_prepare_messages.len(), self.received_commit_messages.len());
            match self.pre_prepare_message.as_ref() {
                None => { println!("noch keine PrePrepareMessage") }
                Some(mes) => { println!("Hab eine PrePrepareMessage: {:?}", mes) }
            }
            match self.prepare_message.as_ref() {
                None => { println!("noch keine PrepareMessage") }
                Some(mes) => { println!("Hab eine PrepareMessage: {:?}", mes) }
            }
            match self.commit_message.as_ref() {
                None => { println!("noch keine CommitMessage") }
                Some(mes) => { println!("Hab eine CommitMessage: {:?}", mes) }
            }
        }
    }
}
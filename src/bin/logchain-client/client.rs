#[macro_use]
extern crate log;

use std::process;
use std::sync::{Arc, Mutex};
use local_ipaddress;
use libp2p::{
    core::Multiaddr,
    PeerId,
    Swarm,
    NetworkBehaviour,
    identity::Keypair,
    floodsub::{self, Floodsub, FloodsubEvent},
    swarm::NetworkBehaviourEventProcess,
    ping::{self, Ping, PingConfig, PingEvent},
};

use std::collections;
use std::fs;
use std::io;
use std::io::{BufReader, Read, Write};
use std::net::{SocketAddr, IpAddr, AddrParseError};
use std::str;
use crate::logblock::block::Entry;
use env_logger;
use libp2p::identify::{IdentifyEvent, Identify};
use libp2p::floodsub::{Topic};
use libp2p::ping::{PingSuccess, PingFailure};
use std::collections::HashMap;
use libp2p::identity::ed25519::{Keypair as EDKeyPair, PublicKey as EDPublicKey};
use libp2p::core::PublicKey;
use std::thread;
use futures::{future, prelude::*};
use std::fs::{File, OpenOptions};
use async_std::task;
use std::{error::Error, task::{Context, Poll}};
use serde::{Deserialize, Serialize};
use directories::{ProjectDirs};
use std::path::{Path, PathBuf};
use std::str::FromStr;

#[path = "../../logchainnetwork.rs"]
mod logchainnetwork;

use logchainnetwork::logchain_context::MAX_BYTES;
use logchainnetwork::logchain_context::SplitMessage;
use logchainnetwork::logchain_messages::OuterMessage;
use logchainnetwork::logchain_messages::RequestMessage;
use logchainnetwork::logchain_messages::ReplyMessage;
use logchainnetwork::logchain_messages::MessageTyp;
use logchainnetwork::logchain_context::ContextReturn;
use logchainnetwork::logchain_context::ContextError;
use rand::Rng;
use rand::distributions::Alphanumeric;
use std::thread::sleep;
use std::time::{Duration, Instant};

#[path = "../../logblockchain.rs"]
mod logblockchain;

#[path = "../../logblock.rs"]
mod logblock;


#[macro_use]
extern crate serde;

const MESSAGES: usize = 100;

#[derive(PartialEq, Eq)]
pub enum ClientState {
    Ready,
    Wait,
}

#[derive(Serialize, Deserialize)]
struct ClientConfig {
    private_key_file: String,
    public_keys_dir: String,
    node_port: u16,
    first_start: bool,
    nodes: Vec<Multiaddr>,
}

struct ClientRuntimeConfig<'a> {
    private_key_file: Vec<u8>,
    public_keys_dir: &'a Path,
    node_port: u16,
    nodes: Vec<Multiaddr>,
    peers: HashMap<PeerId, PublicKey>,
}

impl<'a> ClientRuntimeConfig<'a> {
    pub fn create_from_config(conf: &'a ClientConfig) -> Self {
        let mut private_key_file: Vec<u8> = vec![];
        let mut pkf = OpenOptions::new().
            read(true).
            create(true).
            write(true).
            open(&conf.private_key_file).expect("Error open private key file");
        pkf.read_to_end(&mut private_key_file).unwrap_or(0);
        let public_keys_dir = Path::new(&conf.public_keys_dir);
        if !public_keys_dir.exists() || !public_keys_dir.is_dir() {
            panic!("Public key dir don't exsit or is no dir");
        }
        ClientRuntimeConfig {
            private_key_file,
            public_keys_dir,
            node_port: conf.node_port,
            nodes: conf.nodes.clone(),
            peers: HashMap::new(),
        }
    }
}

impl std::default::Default for ClientConfig {
    fn default() -> Self {
        let proj_dir = match ProjectDirs::from("de", "Fraunhofer", "LogChainClient") {
            None => { panic!("Can not determine Project dirs"); }
            Some(pd) => { pd }
        };
        let cnf_dir = proj_dir.config_dir();
        let local_data_dir = proj_dir.data_local_dir();
        let private_key_path = cnf_dir.join("private.ed25519");
        let public_keys_dir_path = local_data_dir.join("public_keys");

        std::fs::create_dir_all(cnf_dir).expect("Can not create config dir.");
        std::fs::create_dir_all(local_data_dir).expect("Can not create data dir.");
        std::fs::create_dir_all(public_keys_dir_path.as_path()).expect("Can not create public key dir.");
        ClientConfig {
            private_key_file: private_key_path.to_str().expect("NOT SUPPORTED CHAR(S).").to_owned(),
            public_keys_dir: public_keys_dir_path.to_str().expect("NOT SUPPORTED CHAR(S).").to_owned(),
            node_port: 2020,
            first_start: true,
            nodes: vec![],
        }
    }
}

pub struct ClientContext {
    state: ClientState,
    own_key_pair: Keypair,
    public_keys: Vec<PublicKey>,
    topic: Topic,
    split_message_map: HashMap<PeerId, (Vec<Vec<u8>>, u8)>,
    responses: HashMap<u32, (u8, bool)>,
    time_mes: Option<Instant>,
    message_id: u32,
}

impl ClientContext {
    pub fn new(own_key_pair: Keypair, topic: Topic, pub_key_path: &Path) -> ClientContext {
        let mut public_keys = vec![];
        let pub_dir = fs::read_dir(pub_key_path);
        for file in pub_dir.unwrap() {
            let new_pub_key_file = File::open(file.unwrap().path());
            match new_pub_key_file {
                Ok(mut pub_file) => {
                    let mut pub_key_as_bytes: Vec<u8> = vec![];
                    pub_file.read_to_end(&mut pub_key_as_bytes).unwrap_or(0);
                    let pk = EDPublicKey::decode(pub_key_as_bytes.as_slice());
                    match pk {
                        Ok(ppk) => {
                            public_keys.push(PublicKey::Ed25519(ppk));
                        }
                        Err(_) => println!("Error loading file as pub key")
                    }
                }
                Err(_) => println!("Error loading file as pub key")
            }
        }
        ClientContext {
            state: ClientState::Wait,
            own_key_pair: own_key_pair.clone(),
            public_keys,
            topic: topic.clone(),
            split_message_map: HashMap::default(),
            responses: Default::default(),
            time_mes: None,
            message_id: 0
        }
    }
    pub fn is_ready(&self) -> bool {
        self.state.eq(&ClientState::Ready)
    }
    pub fn increment_message_id(&mut self) -> u32 {
        let re = self.message_id;
        self.message_id = re + 1;
        re
    }
    pub fn get_topic_clone(&self) -> Topic {
        self.topic.clone()
    }
    pub fn client_floodsub_publish(topic: &Topic, floodsub: &mut Floodsub, data: Vec<u8>, id: u32) {
        let mut i = 0;
        let data_to_send = data.chunks(MAX_BYTES);
        let end = data_to_send.len() as u8;
        for chunk in data_to_send {
            let split_message = SplitMessage {
                id,
                index: i,
                end,
                data: Vec::from(chunk),
            };
            i = i + 1;
            let bytes_to_send = serde_cbor::to_vec(&split_message);
            match bytes_to_send {
                Ok(bytes) => {
                    floodsub.publish(topic.clone(), bytes);
                }
                Err(_) => { println!("Parsing error") }
            }
        }
    }
    pub fn append_split_message(&mut self, mut split_message: SplitMessage, peer: PeerId) -> Option<OuterMessage> {
        let is_data = self.split_message_map.get_mut(&peer);
        match is_data {
            None => {
                let mut tmp_vec = vec![vec![]; split_message.end as usize];
                if let Some(elem) = tmp_vec.get_mut(split_message.index as usize) {
                    *elem = split_message.data;
                }
                self.split_message_map.insert(peer, (tmp_vec, 1));
            }
            Some(data) => {
                if let Some(elem) = data.0.get_mut(split_message.index as usize) {
                    *elem = split_message.data;
                    data.1 = data.1 + 1;
                }
            }
        }
        if self.split_message_map.get(&peer).unwrap_or(&(vec![vec![0 as u8]], 0 as u8)).1 == (split_message.end) {
            let outer_message_data_opt = self.split_message_map.get_mut(&peer);
            match outer_message_data_opt {
                None => { println!("No Data") }
                Some(mut data) => {
                    let mut datas = vec![];
                    data.0.iter_mut().for_each(|ele| datas.append(ele));
                    let outer_mess = serde_cbor::from_slice::<OuterMessage>(datas.as_slice());
                    match outer_mess {
                        Ok(message) => {
                            self.split_message_map.remove(&peer);
                            return Some(message);
                        }
                        Err(_) => println!("Error parsing")
                    }
                }
            }
        }
        None
    }
    pub fn process_message(&mut self, message: &OuterMessage, floodsub: &mut Floodsub) -> Result<ContextReturn, ContextError> {
        if message.calculate_mac().ne(&message.message_authentication_code) {
            return Err(ContextError::MessageAuthenticationCodeCheckFailed);
        }
        match self.state {
            ClientState::Ready => {
                match message.get_message_typ() {
                    MessageTyp::Reply => {
                        Ok(ContextReturn::ProbablyOldMessage)
                    }
                    _ => {
                        Ok(ContextReturn::StateUp)
                    }
                }
            }
            ClientState::Wait => {
                match message.get_message_typ() {
                    MessageTyp::Reply => {
                        let reply_message = message.to_reply_message();
                        match reply_message {
                            None => {}
                            Some(rep) => {
                                match self.responses.get_mut(&rep.id) {
                                    None => {
                                        self.responses.insert(rep.id, (1, false));
                                        println!("Stand 1")
                                    }
                                    Some(x) => {
                                        x.0 = x.0 + 1;
                                        if x.0 >= 1 && !(x.1) {
                                            self.state = ClientState::Ready;
                                            x.1 = true;
                                            println!("State resetet");
                                        }
                                        println!("Stand {}", x.0);
                                    }
                                }
                            }
                        }
                        Ok(ContextReturn::StateUp)
                    }
                    _ => {
                        Ok(ContextReturn::StateUp)
                    }
                }
            }
        }
    }
}


#[derive(NetworkBehaviour)]
struct ClientBehaviour {
    floodsub: Floodsub,
    identify: Identify,
    ping: Ping,
    #[behaviour(ignore)] ctx: ClientContext,
    #[behaviour(ignore)] queue: Arc<Mutex<Vec<RequestMessage>>>,
}

impl NetworkBehaviourEventProcess<FloodsubEvent> for ClientBehaviour {
    fn inject_event(&mut self, event: FloodsubEvent) {
        match event {
            FloodsubEvent::Message(message) => {
                let out_mes = serde_cbor::from_slice::<SplitMessage>(message.data.as_slice());
                let com_mes = match out_mes {
                    Ok(outer) => {
                        match self.ctx.append_split_message(outer, message.source) {
                            None => { None }
                            Some(mes) => { Some(mes) }
                        }
                    }
                    _ => { None }
                };
                match com_mes {
                    None => {}
                    Some(mes) => {
                        let res = self.ctx.process_message(&mes, &mut self.floodsub);
                        match res {
                            Ok(x) => {}
                            Err(x) => {
                                match x {
                                    ContextError::NotExpectedMessage => { println!("NEM") }
                                    ContextError::MessageAuthenticationCodeCheckFailed => { println!("MACCF") }
                                    ContextError::VerifyFailed => { println!("VF") }
                                    ContextError::ProbablyMaliciousMessage => { println!("PMM") }
                                    ContextError::SigningFailed => { println!("SF") }
                                }
                            }
                        }
                    }
                }
            }
            FloodsubEvent::Subscribed { .. } => {}
            FloodsubEvent::Unsubscribed { .. } => {}
        }
    }
}

impl NetworkBehaviourEventProcess<IdentifyEvent> for ClientBehaviour {
    fn inject_event(&mut self, event: IdentifyEvent) {
        match event {
            IdentifyEvent::Sent { peer_id } => {
                self.floodsub.add_node_to_partial_view(peer_id);
            }
            IdentifyEvent::Received { peer_id, observed_addr, .. } => {
                //println!("Addresse {}", observed_addr);
                self.floodsub.add_node_to_partial_view(peer_id.clone());
                //self.connections.insert(peer_id.clone(), (observed_addr, true)); auf ctx umleiten
            }
            IdentifyEvent::Error { peer_id, error } => {
                self.floodsub.remove_node_from_partial_view(&peer_id);
                println!("Fehler mit {} Fehler {}", peer_id, error)
            }
        }
    }
}

impl NetworkBehaviourEventProcess<PingEvent> for ClientBehaviour {
    // Called when `ping` produces an event.
    fn inject_event(&mut self, event: PingEvent) {
        use ping::handler::{PingFailure, PingSuccess};
        match event {
            PingEvent {
                peer,
                result: Result::Ok(PingSuccess::Ping { rtt }),
            } => {
                /* println!(
                     "ping: rtt to {} is {} ms",
                     peer.to_base58(),
                     rtt.as_millis()
                 );*/
            }
            PingEvent {
                peer,
                result: Result::Ok(PingSuccess::Pong),
            } => {
                //println!("ping: pong from {}", peer.to_base58());
            }
            PingEvent {
                peer,
                result: Result::Err(PingFailure::Timeout),
            } => {
                println!("ping: timeout to {}", peer.to_base58());
            }
            PingEvent {
                peer,
                result: Result::Err(PingFailure::Other { error }),
            } => {
                println!("ping: failure with {}: {}", peer.to_base58(), error);
            }
        }
    }
}

fn create_config() -> ClientConfig {
    let proj_dir = match ProjectDirs::from("de", "Fraunhofer", "LogChainClient") {
        None => { panic!("Can not determine Project dirs"); }
        Some(pd) => { pd }
    };
    let mut cfg: ClientConfig = confy::load_path(proj_dir.config_dir().join("config.toml")).expect("Error reading conf file");
    if cfg.first_start {
        let mut private_key_file = File::create(cfg.private_key_file.clone()).expect("Ca");
        let id_keys = EDKeyPair::generate();
        private_key_file.write(&id_keys.encode()).expect("Error writing key file");
        let local_peer_id = PeerId::from(Keypair::Ed25519(id_keys.clone()).public());
        let mut public_key_file = File::create(Path::new(&cfg.public_keys_dir).join(local_peer_id.to_string()).with_extension("pub.ed25519")).expect("Error crete pub file");
        public_key_file.write(&id_keys.public().encode()).expect("Error writing public key file");
        cfg.first_start = false;
        let addr: Multiaddr = "/ip4/192.168.0.161/tcp/2020".parse().expect("d");
        cfg.nodes.push(addr);
    }
    confy::store_path(proj_dir.config_dir().join("config.toml"), &cfg).expect("Error writing conf file");
    cfg
}

pub fn main() {
    let local_ip_result = IpAddr::from_str(local_ipaddress::get().unwrap().as_str());
    let local_ip = match local_ip_result {
        Ok(ip) => { ip }
        Err(err) => { panic!("{}", err) }
    };

    let floodsub_topic = floodsub::Topic::new("logchain_client");
    let config = create_config();
    let mut runtime_config = ClientRuntimeConfig::create_from_config(&config);
    let mut bytes = runtime_config.private_key_file.clone();
    let id_keys = Keypair::Ed25519(EDKeyPair::decode(&mut bytes).expect("Error reading key file."));
    let local_peer_id = PeerId::from(id_keys.public());
    println!("Local peer id: {:?}", &local_peer_id);
    let mut ctx = ClientContext::new(id_keys.clone(), floodsub_topic.clone(), runtime_config.public_keys_dir);

    let transport = libp2p::build_tcp_ws_noise_mplex_yamux(id_keys.clone()).expect("Can't create p2p network.");

    let mut requestmessages = Arc::new(Mutex::new(vec![]));
    let mut messages = requestmessages.clone();
    let mut swarm = {
        let identify = Identify::new("1.0".to_owned(), "1.0".to_owned(), id_keys.public());
        let ping = Ping::new(PingConfig::new().with_keep_alive(true));
        let mut behaviour = ClientBehaviour {
            floodsub: Floodsub::new(local_peer_id.clone()),
            identify,
            ping,
            ctx,
            queue: requestmessages.clone(),
        };
        behaviour.floodsub.subscribe(floodsub_topic.clone());
        Swarm::new(transport, behaviour, local_peer_id)
    };
    let swarm_addr = format!("/ip4/0.0.0.0/tcp/{}", runtime_config.node_port).parse::<Multiaddr>().expect("Can't parse MultiAddr");
    Swarm::listen_on(&mut swarm, swarm_addr).expect("Can't listen with Swarm");
    // Kick it off
    for addr in &runtime_config.nodes {
        Swarm::dial_addr(&mut swarm, addr.clone()).expect("Error dialing");
        println!("Dialed to {}", addr);
    }
    let mut listening = false;
    let thread_join_receive = thread::spawn(move || {
        sleep(Duration::new(3, 0));
        let mut lock = messages.lock().unwrap();
        while lock.len() < MESSAGES {
            let mut entries = vec![];
            while entries.len() < 1 {
                let mut tmp_string = rand::thread_rng()
                    .sample_iter(&Alphanumeric)
                    .take(12)
                    .map(char::from)
                    .collect();
                entries.push(Entry::new(local_ip.clone(), tmp_string));
            }
            lock.push(RequestMessage::new(entries));
        }
        println!("Nachrichten erstellt");
    });
    let t = task::block_on(future::poll_fn(move |cx: &mut Context<'_>| {
        loop {
            if swarm.ctx.is_ready() {
                match swarm.ctx.time_mes {
                    None => {}
                    Some(time) => {
                        println!("For {} Messages {} µS were needed.", swarm.ctx.responses.len(), time.elapsed().as_micros());
                        if swarm.ctx.responses.len() >= MESSAGES {
                            process::exit(0);
                        }
                    }
                }
                let lock_t = swarm.queue.clone();
                let mut lock = lock_t.lock().unwrap();
                match lock.pop() {
                    None => {}
                    Some(mes) => {
                        match swarm.ctx.time_mes {
                            None => { swarm.ctx.time_mes = Some(Instant::now()) }
                            Some(_) => {}
                        }
                        let option_outer_message = mes.to_outer_message(&swarm.ctx.own_key_pair);
                        if let Ok(outer_message) = option_outer_message {
                            let data = serde_cbor::to_vec(&outer_message).expect("Error message");
                            let message_id = swarm.ctx.increment_message_id();
                            ClientContext::client_floodsub_publish(&swarm.ctx.get_topic_clone(), &mut swarm.floodsub, data, message_id);
                            swarm.ctx.state = ClientState::Wait;
                            println!("Nachricht verschickt");
                        } else { error!("Error on creating OuterMessage, probably signing") }
                    }
                }
            }

            match swarm.poll_next_unpin(cx) {
                Poll::Ready(Some(event)) => println!("{:?}", event),
                Poll::Ready(None) => return Poll::Ready(Ok::<(), Box<dyn Error>>(())),
                Poll::Pending => {
                    if !listening {
                        for addr in Swarm::listeners(&swarm) {
                            println!("Listening on {:?}", addr);
                            listening = true;
                        }
                        swarm.ctx.state = ClientState::Ready;
                    }
                    break;
                }
            }
        }
        Poll::Pending
    }));
}
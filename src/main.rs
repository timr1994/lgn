#[macro_use]
extern crate log;

mod logchainnetwork;
mod logblockchain;
mod logblock;


use std::thread;
use std::sync::{Arc, Mutex};
use futures::{future, prelude::*};
use std::fs::{File, OpenOptions};
use libp2p::{
    core::Multiaddr,
    PeerId,
    Swarm,
    NetworkBehaviour,
    identity::Keypair,
    floodsub::{self, Floodsub, FloodsubEvent},
    swarm::NetworkBehaviourEventProcess,
    ping::{self, Ping, PingConfig, PingEvent},
};
use async_std::task;
use std::{error::Error, task::{Context, Poll}};
use serde::{Deserialize, Serialize};
use directories::{ProjectDirs};
use libp2p::identity::ed25519::{Keypair as EDKeyPair, PublicKey as EDPublicKey};
use std::path::{Path, PathBuf};
use libp2p::identify::{Identify, IdentifyEvent};
use std::collections::HashMap;
use libp2p::identity::PublicKey;
use async_std::net::{SocketAddr, IpAddr};
use std::fs;
use rustls;
use rustls::{RootCertStore, AllowAnyAuthenticatedClient, AllowAnyAnonymousOrAuthenticatedClient, NoClientAuth};
use std::io::{Write, Read, BufReader};
use crate::logchainnetwork::logchain_messages::{RequestMessage};
use crate::logchainnetwork::logchain_context::{LogChainContext, SplitMessage, ContextError, ContextReturn};
use crate::logblockchain::blockchain_wrapper::{BlockchainWrapper, SuperBlockchainWrapper};
use crate::logchainnetwork::logchain_messages::OuterMessage;
use crate::logchainnetwork::logchain_context::ContextState;

// Token for our listening socket.
const LISTENER: mio::Token = mio::Token(0);

#[derive(Serialize, Deserialize)]
struct LogChainConfig {
    private_key_file: String,
    public_keys_dir: String,
    blockchain_file: String,
    node_port: u16,
    first_start: bool,
    nodes: Vec<Multiaddr>,
    node_priority: Vec<String>,
}

struct LogChainRuntimeConfig<'a> {
    private_key_file: Vec<u8>,
    public_keys_dir: &'a Path,
    blockchain_file: PathBuf,
    node_port: u16,
    nodes: Vec<Multiaddr>,
    peers: HashMap<PeerId, PublicKey>,
    node_priority: Vec<String>,
}

impl<'a> LogChainRuntimeConfig<'a> {
    pub fn create_from_config(conf: &'a LogChainConfig) -> Self {
        let mut private_key_file: Vec<u8> = vec![];
        let mut pkf = OpenOptions::new().
            read(true).
            create(true).
            write(true).
            open(&conf.private_key_file).expect("Error open private key file");
        pkf.read_to_end(&mut private_key_file).unwrap_or(0);

        let mut blockchain_file = Path::new(&conf.blockchain_file).join("logchain.blockchain");

        let public_keys_dir = Path::new(&conf.public_keys_dir);
        if !public_keys_dir.exists() || !public_keys_dir.is_dir() {
            panic!("Public key dir don't exsit or is no dir");
        }
        LogChainRuntimeConfig {
            private_key_file,
            public_keys_dir,
            blockchain_file,
            node_port: conf.node_port,
            nodes: conf.nodes.clone(),
            peers: HashMap::new(),
            node_priority: conf.node_priority.clone(),
        }
    }

    pub fn load_public_key(&mut self, peer_id: &PeerId) -> Option<&PublicKey> {
        if self.peers.contains_key(peer_id) {
            return self.peers.get(peer_id);
        } else {
            let new_pub_key_file = File::open(self.public_keys_dir.join(peer_id.to_base58()).with_extension("pub.ed25519"));
            match new_pub_key_file {
                Ok(mut pub_file) => {
                    let mut pub_key_as_bytes: Vec<u8> = vec![];
                    pub_file.read_to_end(&mut pub_key_as_bytes).unwrap_or(0);
                    let pk = EDPublicKey::decode(pub_key_as_bytes.as_slice());
                    match pk {
                        Ok(ppk) => {
                            self.peers.insert(peer_id.clone(), PublicKey::Ed25519(ppk));
                            return self.peers.get(peer_id);
                        }
                        Err(_) => return None
                    }
                }
                Err(_) => return None
            }
        }
    }
    pub fn get_all_nodes(&self) -> Vec<Multiaddr> {
        self.nodes.clone()
    }
}

impl std::default::Default for LogChainConfig {
    fn default() -> Self {
        let proj_dir = match ProjectDirs::from("de", "Fraunhofer", "LogChain") {
            None => { panic!("Can not determine Project dirs"); }
            Some(pd) => { pd }
        };
        let cnf_dir = proj_dir.config_dir();
        let local_data_dir = proj_dir.data_local_dir();
        let private_key_path = cnf_dir.join("private.ed25519");
        let public_keys_dir_path = local_data_dir.join("public_keys");

        let blockchain_dir_path = local_data_dir.join("blockchain");
        let tls_path = proj_dir.config_dir().join("tls");

        std::fs::create_dir_all(cnf_dir).expect("Can not create config dir.");
        std::fs::create_dir_all(local_data_dir).expect("Can not create data dir.");
        std::fs::create_dir_all(public_keys_dir_path.as_path()).expect("Can not create public key dir.");
        std::fs::create_dir_all(blockchain_dir_path.as_path()).expect("Can not create blockchain dir.");
        LogChainConfig {
            private_key_file: private_key_path.to_str().expect("NOT SUPPORTED CHAR(S).").to_owned(),
            public_keys_dir: public_keys_dir_path.to_str().expect("NOT SUPPORTED CHAR(S).").to_owned(),
            blockchain_file: blockchain_dir_path.to_str().expect("NOT SUPPORTED CHAR(S).").to_owned(),
            node_port: 2020,
            first_start: true,
            nodes: vec![],
            node_priority: vec![],
        }
    }
}

#[derive(NetworkBehaviour)]
struct MyBehaviour {
    floodsub: Floodsub,
    identify: Identify,
    ping: Ping,
    #[behaviour(ignore)] ctx: logchainnetwork::logchain_context::LogChainContext,
    #[behaviour(ignore)] queue: Vec<RequestMessage>,
}

impl NetworkBehaviourEventProcess<FloodsubEvent> for MyBehaviour {
    fn inject_event(&mut self, event: FloodsubEvent) {
        match event {
            FloodsubEvent::Message(message) => {
                let out_mes = serde_cbor::from_slice::<SplitMessage>(message.data.as_slice());
                let com_mes = match out_mes {
                    Ok(outer) => {
                        match self.ctx.append_split_message(outer, message.source) {
                            None => { None }
                            Some(mes) => { Some(mes) }
                        }
                    }
                    _ => { None }
                };
                let lock = self.ctx.mutex.clone();
                let real_lock = lock.lock();
                if message.topics.iter().any(|x| x.eq(&self.ctx.get_topic_client_clone())) && self.ctx.am_i_primary() {
                    //println!("Bin logchain_client");
                    match com_mes {
                        None => {}
                        Some(outer) => {
                            match self.ctx.process_message(&outer, &mut self.floodsub) {
                                Ok(cr) => {
                                    match cr {
                                        ContextReturn::StateUp => { /* Process succesful */ }
                                        ContextReturn::ProbablyOldMessage => { println!("Got an Old message that ist not needed, because already handled") }
                                    }
                                }
                                Err(ce) => {
                                    match ce {
                                        ContextError::NotExpectedMessage => { /*println!("Got an not expected message__client")*/ }
                                        ContextError::MessageAuthenticationCodeCheckFailed => { println!(" MAC check failed") }
                                        ContextError::VerifyFailed => { println!("Verfiy failed") }
                                        ContextError::ProbablyMaliciousMessage => { println!("Seems to be an Malicious Message") }
                                        ContextError::SigningFailed => { println!("Signing failed") }
                                    }
                                }
                            }
                        }
                    }
                } else if message.topics.iter().any(|x| x.eq(&self.ctx.get_topic_clone())) {
                    match com_mes {
                        None => {}
                        Some(outer) => {
                            match self.ctx.process_message(&outer, &mut self.floodsub) {
                                Ok(cr) => {
                                    match cr {
                                        ContextReturn::StateUp => { /* Process succesful */ }
                                        ContextReturn::ProbablyOldMessage => { println!("Got an Old message that ist not needed, because already handled") }
                                    }
                                }
                                Err(ce) => {
                                    if self.ctx.state.eq(&ContextState::Commit) { self.ctx.reset_state(); }

                                    match ce {
                                        ContextError::NotExpectedMessage => { println!("Got an not expected message__nor") }
                                        ContextError::MessageAuthenticationCodeCheckFailed => { println!(" MAC check failed") }
                                        ContextError::VerifyFailed => { println!("Verfiy failed") }
                                        ContextError::ProbablyMaliciousMessage => { println!("Seems to be an Malicious Message") }
                                        ContextError::SigningFailed => { println!("Signing failed") }
                                    }
                                }
                            }
                        }
                    }
                }
                real_lock.unwrap();
            }
            FloodsubEvent::Subscribed { .. } => {}
            FloodsubEvent::Unsubscribed { .. } => {}
        }
    }
}

impl NetworkBehaviourEventProcess<IdentifyEvent> for MyBehaviour {
    fn inject_event(&mut self, event: IdentifyEvent) {
        match event {
            IdentifyEvent::Sent { peer_id } => {
                self.floodsub.add_node_to_partial_view(peer_id);
            }
            IdentifyEvent::Received { peer_id, observed_addr, .. } => {
                //println!("Addresse {}", observed_addr);
                self.floodsub.add_node_to_partial_view(peer_id.clone());
                //self.connections.insert(peer_id.clone(), (observed_addr, true)); auf ctx umleiten
            }
            IdentifyEvent::Error { peer_id, error } => {
                self.floodsub.remove_node_from_partial_view(&peer_id);
                println!("Fehler mit {} Fehler {}", peer_id, error)
            }
        }
    }
}

impl NetworkBehaviourEventProcess<PingEvent> for MyBehaviour {
    // Called when `ping` produces an event.
    fn inject_event(&mut self, event: PingEvent) {
        use ping::handler::{PingFailure, PingSuccess};
        match event {
            PingEvent {
                peer,
                result: Result::Ok(PingSuccess::Ping { rtt }),
            } => {
                self.ctx.set_peer_online(peer);
            }
            PingEvent {
                peer,
                result: Result::Ok(PingSuccess::Pong),
            } => {
                self.ctx.set_peer_online(peer);
            }
            PingEvent {
                peer,
                result: Result::Err(PingFailure::Timeout),
            } => {
                self.ctx.set_peer_offline(peer);
            }
            PingEvent {
                peer,
                result: Result::Err(PingFailure::Other { error }),
            } => {
                self.ctx.set_peer_offline(peer);
            }
        }
    }
}

fn main() {
    let config = create_config();
    let mut runtime_config = LogChainRuntimeConfig::create_from_config(&config);
    let mut blch = SuperBlockchainWrapper::new(runtime_config.blockchain_file.as_path());
    let mut bytes = runtime_config.private_key_file.clone();
    let id_keys = Keypair::Ed25519(EDKeyPair::decode(&mut bytes).expect("Error reading key file."));//EDKeypair::decode(&mut bytes).unwrap();
    let local_peer_id = PeerId::from(id_keys.public());
    println!("Local peer id: {:?}", &local_peer_id);
    if runtime_config.nodes.len() < 3 {
        panic!("Not engouh nodes, Config is written")
    }
    let response_list = Arc::new(Mutex::new(vec![]));
    let response_list_ssl = response_list.clone();
    //Maybe libp2p::build_tcp_ws_pnet_noise_mplex_yamux implementation is more interesting because of the pre-shared key.
    let transport = libp2p::build_tcp_ws_noise_mplex_yamux(id_keys.clone()).expect("Can't create p2p network.");
    // Create a Floodsub topic
    let floodsub_topic = floodsub::Topic::new("logchain");
    let floodsub_topic_client = floodsub::Topic::new("logchain_client");
    let primary = runtime_config.node_priority.get(0).unwrap_or_else(|| panic!("Could not get Primary")).clone();
    println!("Primary node: {}", primary);
    let mut ctx = LogChainContext::new(blch, id_keys.clone(), floodsub_topic.clone(), floodsub_topic_client.clone(), runtime_config.public_keys_dir, response_list, primary, runtime_config.node_priority);
    // Create a Swarm to manage peers and events

    let mut swarm = {
        let identify = Identify::new("1.0".to_owned(), "1.0".to_owned(), id_keys.public());
        let ping = Ping::new(PingConfig::new().with_keep_alive(true));
        let mut behaviour = MyBehaviour {
            floodsub: Floodsub::new(local_peer_id.clone()),
            identify,
            ping,
            ctx,
            queue: vec![],
        };
        behaviour.floodsub.subscribe(floodsub_topic.clone());
        behaviour.floodsub.subscribe(floodsub_topic_client.clone());
        Swarm::new(transport, behaviour, local_peer_id)
    };
    let swarm_addr = format!("/ip4/0.0.0.0/tcp/{}", runtime_config.node_port).parse::<Multiaddr>().expect("Can't parse MultiAddr");
    Swarm::listen_on(&mut swarm, swarm_addr).expect("Can't listen with Swarm");
    // Kick it off
    for addr in &runtime_config.nodes {
        Swarm::dial_addr(&mut swarm, addr.clone()).expect("Error dialing");
        //println!("Dialed to {}",addr);
    }
    let mut listening = false;
    let t = task::block_on(future::poll_fn(move |cx: &mut Context<'_>| {
        loop {
            match swarm.poll_next_unpin(cx) {
                Poll::Ready(Some(event)) => println!("{:?}", event),
                Poll::Ready(None) => return Poll::Ready(Ok::<(), Box<dyn Error>>(())),
                Poll::Pending => {
                    if !listening {
                        for addr in Swarm::listeners(&swarm) {
                            println!("Listening on {:?}", addr);
                            listening = true;
                        }
                    }
                    break;
                }
            }
        }
        Poll::Pending
    }));
}

fn create_config() -> LogChainConfig {
    let proj_dir = match ProjectDirs::from("de", "Fraunhofer", "LogChain") {
        None => { panic!("Can not determine Project dirs"); }
        Some(pd) => { pd }
    };
    let mut cfg: LogChainConfig = confy::load_path(proj_dir.config_dir().join("config.toml")).expect("Error reading conf file");
    if cfg.first_start {
        //Create Key
        let mut private_key_file = File::create(cfg.private_key_file.clone()).expect("Ca");

        let id_keys = EDKeyPair::generate();
        private_key_file.write(&id_keys.encode()).expect("Error writing key file");
        let local_peer_id = PeerId::from(Keypair::Ed25519(id_keys.clone()).public());
        let mut public_key_file = File::create(Path::new(&cfg.public_keys_dir).join(local_peer_id.to_string()).with_extension("pub.ed25519")).expect("Error crete pub file");
        public_key_file.write(&id_keys.public().encode()).expect("Error writing public key file");
        cfg.first_start = false;
        let addr: Multiaddr = "/ip4/192.168.0.161/tcp/2020".parse().expect("d");
        cfg.nodes.push(addr);
    }
    confy::store_path(proj_dir.config_dir().join("config.toml"), &cfg).expect("Error writing conf file");
    cfg
}





pub mod block {
    use sha3::{Digest, Sha3_512};
    use serde::{Deserialize, Serialize};
    use std::net::IpAddr;
    use chrono::Utc;
    use std::fmt::{Display, Formatter};

    /// Type for Hash, in use is SHA3 where we need 32 Bytes
    pub type BlockHash = Vec<u8>;

    pub fn printHex_Blockhash(blockhash: BlockHash) {
        for b in blockhash {
            print!("{:2x}",b)
        }
    }

    pub type Entries = Vec<Entry>;

    /// Entry in side the Blocks
    #[derive(Debug, Serialize, Deserialize, Clone)]
    pub struct Entry {
        /// IP address from the source client
        pub source: IpAddr,
        /// The given log message
        pub message: String,
        /// The hash of the log message
        #[serde(with = "serde_bytes")]
        pub entry_hash: Vec<u8>,
        // true if message ist encrypted
        // pub encrypted: bool,
        // Key identifier for the encryption
        // pub key_id: String
    }

    impl Entry {
        pub fn new(source: IpAddr, message: String) -> Self {
            let mut hasher = Sha3_512::new();
            hasher.update(message.as_bytes());
            let entry_hash = hasher.finalize().to_vec();
            Entry {
                source,
                message,
                entry_hash,
            }
        }
        pub fn to_bytes(&self) -> Vec<u8> {
            let mut bytes_vec: Vec<u8> = vec![];
            let ip_addr_bytes: Vec<u8> = match self.source {
                IpAddr::V4(ip) => ip.octets().to_vec(),
                IpAddr::V6(ip) => ip.octets().to_vec()
            };
            bytes_vec.extend(ip_addr_bytes.iter().copied());
            bytes_vec.extend_from_slice(self.message.as_bytes());
            bytes_vec.extend(self.entry_hash.iter().copied());
            bytes_vec
        }
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct Block {
        /// The timestamp of the Block in non-leap-milliseconds since January 1, 1970 UTC.
        pub timestamp: i64,
        /// Index of the Block in the Chain
        pub index: u32,
        /// Hash of the previous Block
        #[serde(with = "serde_bytes")]
        pub prev_hash: BlockHash,
        /// Vector with the Logentries
        pub entries: Entries,
        /// Typ
        pub typ: EntryBlockTyp,
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct Superblock {
        /// The timestamp of the Block in non-leap-milliseconds since January 1, 1970 UTC.
        pub timestamp: i64,
        /// Index of the Block in the Chain
        pub index: u32,
        /// Hash of the previous Block
        #[serde(with = "serde_bytes")]
        pub prev_hash: BlockHash,
        /// The Termnialblock
        pub terminal_block: Block,
    }

    #[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
    pub enum EntryBlockTyp {
        Genesis,
        Terminal,
        Normal,
    }

    impl Superblock {
        pub fn new(index: u32, timestamp: i64, prev_hash: BlockHash, terminal_block: Block) -> Self {
            Superblock {
                timestamp,
                index,
                prev_hash,
                terminal_block,
            }
        }
        pub fn hash_block(&self) -> BlockHash {
            let mut hasher = Sha3_512::new();
            hasher.update(self.timestamp.to_le_bytes());
            hasher.update(self.index.to_le_bytes());
            hasher.update(self.prev_hash.as_slice());
            hasher.update(self.terminal_block.hash_block());
            hasher.finalize().to_vec()
        }
    }

    impl Block {
        pub fn new_normal(index: u32, timestamp: i64, prev_hash: BlockHash, entries: Entries) -> Self {
            Block {
                timestamp,
                index,
                prev_hash,
                entries,
                typ: EntryBlockTyp::Normal,
            }
        }

        pub fn new_genesis(index: u32, timestamp: i64, entries: Entries) -> Self {
            Block {
                timestamp,
                index,
                prev_hash: vec![0; 64],
                entries,
                typ: EntryBlockTyp::Genesis,
            }
        }
        pub fn new_terminal(index: u32, timestamp: i64, prev_hash: BlockHash) -> Self {
            Block {
                timestamp,
                index,
                prev_hash,
                entries: vec![],
                typ: EntryBlockTyp::Terminal,
            }
        }

        pub fn hash_block(&self) -> BlockHash {
            let mut hasher = Sha3_512::new();
            hasher.update(self.timestamp.to_le_bytes());
            hasher.update(self.index.to_le_bytes());
            hasher.update(self.prev_hash.as_slice());
            self.entries.iter().for_each(|x|
                hasher.update(x.to_bytes())
            );
            hasher.finalize().to_vec()
        }

        pub fn get_timestamp(&self) -> i64 {
            self.timestamp
        }

        pub fn get_index(&self) -> u32 {
            self.index
        }

        pub fn get_prev_hash(&self) -> BlockHash {
            self.prev_hash.clone()
        }
    }
}
